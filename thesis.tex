% !TeX encoding = UTF-8
% !TeX program = pdflatex
% !TeX spellcheck = it_IT

\documentclass[binding=0.6cm,Lau]{sapthesis} % LaM for a Laurea Magistrale
\usepackage{microtype}
\usepackage[italian]{babel}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\hypersetup{pdftitle={Una shell remota per sistemi POSIX e POSIX-like},pdfauthor={Andrea Fanti}}
%\usepackage{tikz}
%\usetikzlibrary{automata, positioning, arrows}
%\tikzset{->,>=stealth',node distance=5cm,every state/.style={thick, fill=gray!1},}

\title{Una shell remota per sistemi POSIX e POSIX-like}
\author{Andrea Fanti}
\IDnumber{1650746}
\course{Ingegneria informatica e automatica}
\courseorganizer{Facoltà di Ingegneria dell'informazione, informatica e statistica}
\AcademicYear{2018/2019}
\copyyear{2019}
\advisor{Prof. Giorgio Grisetti}
\authoremail{fanti.1650746@studenti.uniroma1.it}
\website{https://gitlab.com/aueuaio/wsh}

\begin{document}

\frontmatter
\maketitle
% \dedication{Dedicato a\\ Donald Knuth}

\begin{abstract}

Il progetto consiste in una shell testuale per sistemi POSIX e POSIX-like che
supporti l'accesso remoto tramite il tradizionale modello client/server. \`E
stata implementata buona parte delle classiche funzionalit\`a di queste shell
come le pipeline di comandi, la redirezione dell'I/O, l'esecuzione di comandi in
background e un assortimento minimale di comandi built--in che permettano di
navigare tra le directory e di accedere alle variabili d'ambiente. Oltre al
server e al client a interfaccia testuale, \`e stato realizzato un client che
implementa un rudimentale server HTTP e permette quindi di eseguire righe di
comando tramite messaggi di richiesta HTTP e ottenerne l'output come risposta.

\end{abstract}

\tableofcontents

\mainmatter
\chapter{Specifiche}

La shell fornisce le seguenti funzionalit\`a:

\begin{itemize}

\item un insieme minimale di comandi built--in che permettono di leggere e
modificare le variabili d'ambiente e la directory di lavoro;

\item creazione di processi e pipeline di processi sincroni e asincroni alla
shell a partire da eseguibili esterni;

\item redirezione dello standard output o standard error di un comando a un file
presente sulla macchina remota;

\item utilizzo di un file presente sulla macchina remota come standard input di
un comando;

\item inoltro automatico dello standard input dei comandi da client a server e
di standard output e standard error da server a client;

\item autenticazione dell'utente del client quando questo non sia eseguito sulla
stessa macchina del server.

\end{itemize}

Il codice è interamente scritto nel linguaggio C; in particolare, viene
richiesto che il compilatore supporti la versione C99 e che fornisca un
sottoinsieme delle librerie standard POSIX, principalmente relative alla
gestione dei processi, dei segnali e dei descrittori di file. Il client testuale
utilizza inoltre la libreria GNU readline per leggere le righe di comando
dall'utente, mentre il server utilizza la libreria PAM per l'autenticazione.

\section{Eseguibili prodotti e loro utilizzo}

Il codice C del progetto viene compilato tramite un makefile e produce i
seguenti eseguibili:

\begin{description}

\item [\texttt{wshd}] che implementa il lato server operando come un classico
programma "demone", ovvero attendendo richieste di connessioni da client che
saranno poi servite individualmente da suoi processi figli;

\item [\texttt{wsh}] che implementa il lato client con la tradizionale
interfaccia da terminale;

\item [\texttt{wsh-http}] che implementa il lato client con un interfaccia
HTTP, ovvero che attende richieste HTTP che contengano righe di comando da
eseguire e risponde con il loro output.

\end{description}

L'eseguibile \texttt{wshd} accetta un unico argomento da riga di comando che
specifica la porta TCP sulla quale si dovranno attendere connessioni dai client.
Per ogni client accettato verr\`a creato un processo figlio che lo serva, mentre
il processo genitore attende nuove richieste di connessioni da altri client. Si
noti che su molti sistemi (ad es. GNU/linux) va creato un file
\texttt{/etc/pam.d/wsh.conf} per permettere al server di utilizzare la libraria
PAM: il contenuto minimale del file \`e:

\begin{verbatim}
auth       required     pam_unix.so
account    required     pam_unix.so
\end{verbatim}

L'eseguibile \texttt{wsh} accetta due argomenti da riga di comando, che
specificano rispettivamente l'indirizzo IP del server nella notazione "dotted" e
la porta TCP da utilizzare. Una volta connesso col server, l'utente potr\`a
digitare righe di comando dal terminale utilizzando l'interfaccia della GNU
readline. Per terminare la sessione si potr\`a quindi utilizzare, oltre al
comando \texttt{exit}, anche la combinazione di tasti Alt--D su una riga vuota.

L'eseguibile \texttt{wsh-http} accetta due argomenti da riga di comando che
specificano rispettivamente la porta del server della shell e la porta sulla
quale attendere richieste HTTP; per evitare l'autenticazione, viene assunto che
il server sia locale e il suo indirizzo IP sia quindi "127.0.0.1". Viene fornita
la possibilit\`a di attendere richieste HTTP su una porta diversa da quella ad
esso riservata (la 80) poich\'e, sulla maggior parte dei sistemi supportati, per
aprire una socket su questa porta l'eseguibile deve avere i permessi di root. In
alternativa, alcuni sistemi mettono a disposizione utility per dare a un file
eseguibile il permesso specifico di aprire socket su porte riservate: ad
esempio, eseguendo su un sistema GNU/linux il comando

\begin{verbatim}
sudo setcap cap_net_bind_service=+ep ./wsh-http
\end{verbatim}

si pu\`o eseguire \texttt{wsh-http} specificando la porta 80 anche come utenti
diversi da root. Il client HTTP esegue righe di comando che gli arrivino come
contenuti di richieste HTTP di tipo POST e risponde col loro output; non
gestisce l'output dei comandi in background n\'e permette all'utente di mandare
input ad un comando in esecuzione.

In caso di errore che non comprometta la connessione TCP tra client e server, la
parte che rileva l'errore avviser\`a l'altra che sta per terminare la sessione
con uno status di terminazione non nullo e l'altra parte terminer\`a con lo
stesso status di terminazione. Se l'errore ha invece compromesso la connessione
la parte che rileva l'errore terminer\`a senza provare a comunicare
ulteriormente con l'altra.

Per quanto riguarda la sicurezza, va notato che il fatto che il server non
richieda l'autenticazione a client locali di fatto rende possibile che utenti
remoti eseguano righe di comando senza autenticarsi. La responsabilit\`a in
questo caso ricade infatti tutta sul client, che potrebbe connettersi a utenti
remoti evitando l'autenticazione. Questa vulnerabilit\`a \`e comunque limitata
dal fatto che il client debba essere locale: un eventuale attacco presupporrebbe
comunque o la possibilit\`a di installare ed eseguire un client maligno oppure
una falla in un client esistente. Il client HTTP presenta questo problema, che
non \`e stato risolto semplicemente per evitare di dover implementare un sistema
di autenticazione appropriato.


\section{Sintassi e semantica delle righe di comando}

La sintassi delle righe di comando è fortemente ispirata a quella della shell
Bash. Comandi built--in ed esterni hanno le due seguenti forme generali,
espresse in notazione BNF.

\begin{verbatim}
<comando_builtin> ::= <nome_comando_builtin>
                      [ spazi <lista_argomenti> ] [spazi]
                      [ <redirezione_stdio> ]
<comando_esterno> ::= nome_comando
                      [ spazi <lista_argomenti> ]
                      [ <pipeline> ] [spazi]
                      [ <redirezione_stdio> ]
                      ["&"]
\end{verbatim}

dove:

\begin{verbatim}
<nome_comando_builtin> ::= "exit" | "pwd" | "cd"
                         | "get"  | "set" | "unset"
<lista_argomenti>      ::= argomento [ spazi <lista_argomenti> ]
<pipeline>             ::= [spazi] "|" [spazi] nome_comando
                           [ <lista_argomenti> ] [ <pipeline> ]
<readirezione_stdio>   ::= ["<" nome_file] [">" nome_file] [">>" nome_file]
\end{verbatim}

\texttt{spazi} è un sequenza qualsiasi di uno o più caratteri di spaziatura;
\texttt{nome\_comando}, \texttt{argomento} e \texttt{nome\_file} sono sequenze
di caratteri di parola, ovvvero caratteri che non siano spazi o caratteri
speciali. I caratteri speciali sono "\texttt{<}", "\texttt{>}", "\texttt{|}",
"\texttt{\&}", "\texttt{"}" e "\texttt{\textbackslash}". Si pu\`o forzare
l'interpretazione di un singolo carattere qualsiasi come carattere di parola
precedendolo con un carattere "\texttt{\textbackslash}" o di un intera sequenza
di caratteri come caratteri di parola racchiudendoli tra due caratteri
"\texttt{"}". Si noti che righe di comando vuote non sono valide (il client
testuale le intercetta evitando di mandarle al server).

Per comandi esterni, ogni \texttt{nome\_comando} indica un eseguibile sulla
macchina remota, espresso come percorso assoluto o relativo rispetto a una delle
directory di \texttt{PATH}; questo viene eseguito con l'eventuale lista di
argomenti fornita. Per eseguire un comando esterno la shell crea un processo
figlio della shell per ogni \texttt{nome\_comando}; successivamente, ogni
standard output di ogni processo viene rediretto allo standard input del
successivo, ad eccezione del primo processo, che manterr\`a il suo standard
input, e dell'ultimo, che manterr\`a il suo standard output. La shell
attender\`a che tutti i processi figli terminino prima di richiedere una nuova
riga di comando dal client.  Nel caso in cui sia presente il carattere
"\texttt{\&}" alla fine della riga di comando, questo viene eseguito in
background: invece di attenderne la terminazione, la shell richieder\`a
immediatamente una nuova riga da eseguire. Necessariamente ci pu\`o essere un
solo processo che non sia in background, che \`e anche l'unico al quale viene
inoltrato lo standard input ricevuto dall'utente del client. Eventuali
\texttt{<redirezione\_stdio>} presenti hanno l'effetto di sostituire
rispettivamente lo standard input, output o error del comando con il
\texttt{nome\_file} associato; nel caso di pipeline di pi\`u processi questa
redirezione influenzer\`a solo il primo o l'ultimo processo a seconda dei casi.

\subsection{Semantica dei comandi built--in}

\begin{description}

\item [\texttt{exit}] non accetta argomenti e termina la sessione corrente con
successo.

\item [\texttt{pwd}] non accetta argomenti e stampa il percorso della directory
di lavoro corrente della shell.

\item [\texttt{cd}] accetta un singolo argomento che sia il percorso di una
directory e cambia la directory di lavoro corrente della shell al percorso
specificato.

\item [\texttt{get}] accetta un singolo argomento che sia il nome di una
variabile d'ambiente e ne stampa il valore.

\item [\texttt{set}] accetta due argomenti e imposta il valore della variabile
d'ambiente di nome dato dal primo argomento al valore dato come secondo
argomento; se la variabile non è presente nell'ambiente, viene aggiunta.

\item [\texttt{unset}] accetta un singolo argomento che sia il nome di una
variabile d'ambiente e la rimuove dall'ambiente.

\end{description}


\chapter{Struttura del codice sorgente}

Il codice sorgente \`e scritto per lo pi\`u seguendo il paradigma procedurale ed
\`e diviso nei seguenti moduli:

\begin{itemize}

\item il modulo \texttt{client-main.c} che contiene la funzione \texttt{main}
del client testuale;

\item il modulo \texttt{server-main.c} che contiene la funzione \texttt{main}
del server;

\item il modulo \texttt{http-client.c} che contiene la funzione
\texttt{main} del client HTTP;

\item il modulo \texttt{msg\_io.c} che implementa le primitive di scambio dei
messaggi per il protocollo di comunicazione tra client e server;

\item il modulo \texttt{commands\_execution.c} che implementa la logica di
esecuzione di comandi built--in e di pipeline di comandi esterni;

\item il modulo \texttt{parser.c} che implementa il parser delle righe di
comando

\item il modulo \texttt{utils/array\_list.c} che implementa una lista dinamica
con un array come rappresentazione interna.

\item il modulo \texttt{utils/fd\_io.c} che implementa delle funzioni utili per
l'I/O su descrittori di file.

\end{itemize}

Le varie dipendenze sono descritte dal diagramma in figura \ref{fig:srcstruct}.
Si nota in particolare che i client non hanno dipendenze inerenti al parsing o
all'esecuzione delle righe di comando e risultano quindi pi\`u flessibili e
compatti del lato server.

\begin{figure}
\centering
\includegraphics[scale=0.25]{src_struct.png}
\caption{Interfacce e dipendenze dei moduli del progetto}
\label{fig:srcstruct}
\end{figure}


\chapter{Protocollo di comunicazione tra client e server}

Il protocollo applicativo utilizzato per la comunicazione client/server si
appoggia al protocollo TCP; prima di poter comunicare, quindi, client e server
devono stabilire una connessione TCP. Il protocollo si basa sullo scambio di
messaggi composti da:

\begin{itemize}

\item due campi di intestazione di 32 bit l'uno, che specificano (in forma di
intero senza segno) rispettivamente il tipo del messaggio e la lunghezza in byte
del payload

\item un campo di payload, di lunghezza variabile, che viene interpretato, a
seconda del tipo di messaggio, come un intero, una stringa o una semplice
sequenza di caratteri.

\end{itemize}

\section{Tipi di messaggio}

\begin{description}

\item [\texttt{MSG\_EXIT}] viene inviato da una delle due parti per avvisare che
la sessione deve essere terminata. Il payload \`e un intero con segno di 1 byte
che indica lo stato di terminazione appropriato.

\item [\texttt{MSG\_PAM\_PROMPT\_ECHO\_ON} e
\texttt{MSG\_PAM\_PROMPT\_ECHO\_OFF}] vengono inviati dal server durante
l'autenticazione, quando la libreria PAM richiede informazioni testuali
dall'utente rispettivamente con l'echoing attivato o disattivato (i.e. per
inserire la password). Il payload \`e una stringa terminata da un carattere
nullo che va mostrata all'utente del client come prompt.

\item [\texttt{MSG\_PAM\_PROMPT\_RESPONSE}] viene inviato dal client come
risposta ad uno dei due messaggi sopra. Il payload \`e la stringa terminata da
un carattere nullo che contiene la risposta dell'utente da fornire alla libreria
PAM.

\item [\texttt{MSG\_PAM\_TEXT\_INFO} e \texttt{MSG\_PAM\_ERROR\_MSG}] vengono
inviati dal server durante l'autenticazione, quando la libreria PAM richiede che
un messaggio di errore o informativo siano mostrati all'utente. Il payload \`e
una stringa terminata da un carattere nullo che contiene il messaggio in
questione.

\item [\texttt{MSG\_PAM\_AUTHENTICATION\_STATUS}] viene inviato dal server al
termine dell'autenticazione. Il payload \`e un intero con segno di 1 byte che,
se non nullo, indica che l'autenticazione \`e andata a buon fine.

\item [\texttt{MSG\_COMMAND\_LINE\_REQUEST}] viene inviato dal server quando ha
bisogno di una riga di comando dall'utente del client. Il payload \`e una
stringa terminata da un carattere nullo da usare come prompt.

\item [\texttt{MSG\_COMMAND\_LINE\_RESPONSE}] viene inviato dal client quando
l'utente finisce di digitare una riga di comando richiesta dal server. Il
payload \`e una stringa terminata da un carattere nullo che contiene la riga di
comando.

\item [\texttt{MSG\_STDIN}] viene inviato dal client quando l'utente manda
dell'input al client durante l'esecuzione di un comando in foreground. Il
payload \`e la sequenza di caratteri da inoltrare al comando in foreground.

\item [\texttt{MSG\_STDOUT} e \texttt{MSG\_STDERR}] vengono inviati dal server
quando un comando scrive rispettivamente sullo standard output o sullo standard
error. Il payload \`e la sequenza di caratteri da mostrare all'utente.

\end{description}

\section{Fasi del protocollo e sua implementazione}

Il protocollo \`e diviso sostanzialmente in due fasi: la fase preliminare di
autenticazione e la fase principale di esecuzione delle righe di comando. La
fase principale si presta facilmente ad essere implementata tramite un ciclo che
utilizzi la \texttt{pselect} per attendere i messaggi dell'altra parte. Questo
approccio ha il vantaggio di permettere ai client e al server di attendere
contemporaneamente anche input dall'utente (per i client) e output dai comandi
(per il server). L'utilizzo della \texttt{pselect} invece che della
\texttt{select} ha l'ulteriore vantaggio di poter scrivere gestori per i segnali
sapendo che verranno chiamati solo immediatamente dopo che ogni chiamata
\texttt{pselect} termina. L'unico svantaggio di questo approccio \`e che le
varie procedure di elaborazione dei messaggi, dell'input dell'utente, delle
righe di comando e dell'output dei comandi non possono essere bloccanti;
tuttavia, questo risulta in realt\`a essere un problema solo per il client
testuale, nel quale \`e per\`o stato risolto utilizzando l'interfaccia non
bloccante della GNU readline.

Le figure \ref{fig:client_statechart} e \ref{fig:server_statechart} riportano i
diagrammi di stato dei lati client e server, che descrivono i dettagli delle
varie fasi.

\begin{figure}
\centering
\includegraphics[scale=0.33]{client.png}
\caption{Diagramma di stato del lato client}
\label{fig:client_statechart}
\end{figure}

\begin{figure}
\centering
\hspace*{-2.5cm}
\includegraphics[scale=0.27]{server.png}
\caption{Diagramma di stato del lato server}
\label{fig:server_statechart}
\end{figure}

\chapter{Algoritmo di parsing delle righe di comando}

L'algoritmo di parsing delle righe di comando, implementato dalla funzione
\texttt{parse\_command\_line}, \`e scritto adottando un approccio top--down a
singola passata. L'albero sintattico non \`e costruito esplicitamente, ma
vengono invece prodotti come output:

\begin{itemize}

\item un intero che identifichi l'eventuale comando built--in riconosciuto o un
valore speciale che indichi che il comando \`e esterno;

\item un booleano che indica se il comando \`e da eseguire in background o in
foreground (non rilevante nel caso di comandi built--in);

\item tre stringhe che indicano eventuali redirezioni di standard input, output
o error;

\item una lista che contiene liste di stringhe che hanno come primo elemento il
nome del comando e gli argomenti come elementi successivi (per i comandi
built--in questa lista contiene un solo elemento);

\item una lista di interi che per ogni elemento della lista sopra indicano il
conto delle stringhe in essa contenute.

\end{itemize}

Di fatto l'algoritmo \`e analogo a quello di un parser recursive--descent: la
funzione principale chiama infatti varie funzioni ausiliarie che effettuano un
parsing parziale dell'input; ognuna di queste ritorner\`a una sottostringa
dell'input rimanente da fornire ai parser successivi fino a che tutto l'input
non sia consumato o il parsing fallisca. Le figure \ref{fig:parser} e
\ref{fig:parseword} riportano i diagrammi delle attivit\`a della funzione
principale e dell'unica funzione ausiliaria non banale, \texttt{parse\_word}.


\begin{figure}
\centering
\includegraphics[scale=0.22]{parser.png}
\caption{Diagramma di attivit\`a che illustra l'algoritmo del parser. Sono state
omesse le azioni in cui si consumano caratteri di spaziatura e quelle in cui si
producono i vari output per mantenere la leggibilit\`a.}
\label{fig:parser}
\end{figure}


\begin{figure}
\centering
\hspace*{-1cm}
\includegraphics[scale=0.22]{parse_word.png}

\caption{Diagramma di attivit\`a che illustra l'algoritmo per consumare un
singola parola da una stringa di testo, utilizzato dal parser per consumare nomi
dei comandi, argomenti e nomi di file per la redirezione. Si assume che
l'attivit\`a possa chiamare s\'e stessa ricorsivamente. Sono state omesse le
condizioni complementari nei condizionali per mantenere la leggibilit\`a.}

\label{fig:parseword}
\end{figure}

\backmatter
\cleardoublepage
\phantomsection % Give this command only if hyperref is loaded

%\addcontentsline{toc}{chapter}{\bibname}
% Here put the code for the bibliography. You can use BibTeX or
% the BibLaTeX package or the simple environment thebibliography.

\end{document}
