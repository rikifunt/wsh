// Make fdopen available.
#define _POSIX_C_SOURCE 200809L

#include "commands_execution.h"

#include <stdio.h> // FILE*, fprintf, fdopen, fclose, fflush
#include <stdlib.h> // exit, malloc, free, setenv
#include <string.h> // strcmp
#include <errno.h> // errno, strerror
#include <unistd.h> // dup, execvp, fork, chdir, getcwd
#include <assert.h> // assert


static int builtin_exit_main (void (*print)(int, const char*), int argc, char** argv)
{
  print (0, "exit\n");
  return 0;
}

static int builtin_pwd_main (void (*print)(int, const char*), int argc, char** argv)
{
  int status, buffer_capacity = 128;
  char* cwd;
  char* buffer;

  if (argc != 1)
  {
    print (1, "pwd: wrong number of arguments (1 expected)\n");
    return 1;
  }

  buffer = malloc (buffer_capacity);
  if (buffer == NULL)
  {
    print (1, "pwd: virtual memory exhausted\n");
    return 1;
  }

  cwd = getcwd (buffer, buffer_capacity);
  while (cwd == NULL && errno == ERANGE)
  {
    char* buffer_bak = buffer;

    buffer_capacity *= 2;
    buffer = realloc (buffer, buffer_capacity);
    if (buffer == NULL)
    {
      free (buffer_bak);
      print (1, "pwd: virtual memory exhausted\n");
      return 1;
    }

    cwd = getcwd (buffer, buffer_capacity);
  }

  if (cwd == NULL)
  {
    free (buffer);
    print (1, "pwd: ");
    print (1, strerror (errno));
    print (1, "\n");
    return 1;
  }

  print (0, cwd);
  print (0, "\n");

  free (buffer);

  return 0;
}

static int builtin_cd_main (void (*print)(int, const char*), int argc, char** argv)
{
  int status;

  if (argc != 2)
  {
    print (1, "cd: wrong number of arguments (1 expected)\n");
    return 1;
  }

  status = chdir (argv[1]);

  if (status == -1)
  {
    print (1, "cd: ");
    print (1, strerror (errno));
    print (1, "\n");

    return 1;
  }

  return 0;
}

static int builtin_get_main (void (*print)(int, const char*), int argc, char** argv)
{
  char* variable_value;

  if (argc != 2)
  {
    print (1, "get: wrong number of arguments (1 expected)\n");
    return 1;
  }

  variable_value = getenv (argv[1]);
  if (variable_value == NULL)
  {
    print (1, "get: no such environment variable\n");
    return 1;
  }

  print (0, variable_value);
  print (0, "\n");

  return 0;
}

static int builtin_set_main (void (*print)(int, const char*), int argc, char** argv)
{
  int status;

  if (argc != 3)
  {
    print (1, "set: wrong number of arguments (2 expected)\n");
    return 1;
  }

  status = setenv (argv[1], argv[2], 1);
  if (status == -1)
  {
    print (1, "set: ");
    print (1, strerror (errno));
    print (1, "\n");
    return 1;
  }

  return 0;
}

static int builtin_unset_main (void (*print)(int, const char*), int argc, char** argv)
{
  int status;

  if (argc != 2)
  {
    print (1, "unset: wrong number of arguments (1 expected)\n");
    return 1;
  }

  status = unsetenv (argv[1]);
  if (status == -1)
  {
    print (1, "unset: ");
    print (1, strerror (errno));
    print (1, "\n");
    return 1;
  }

  return 0;
}


struct builtin
  { const char* name
  ; int         (*main) (void(*)(int, const char*), int, char**)
  ;};

const int exit_builtin_index = 0;

static const struct builtin builtins[] =
  { { "exit", builtin_exit_main }
  , { "pwd", builtin_pwd_main }
  , { "cd", builtin_cd_main }
  , { "get", builtin_get_main }
  , { "set", builtin_set_main }
  , { "unset", builtin_unset_main }
  , { NULL, NULL }
  };

int get_builtin_index (const char* command_name)
{
  int builtin_index = -1;

  for (int i = 0; builtins[i].name != NULL; i++)
    if (strcmp (command_name, builtins[i].name) == 0)
      builtin_index = i;

  return builtin_index;
}

int execute_builtin
  (int builtin_index, void (*print)(int, const char*), int command_argc, char** command_argv)
{
  int exit_status;

  exit_status =
    builtins[builtin_index].main (print, command_argc, command_argv);

  return exit_status;
}


static void execute_external_command (int (*stdio_fds_)[3], char** argv)
{
  //TODO Do we need to reset signal handling? Probably not since handled signals
  //are blocked outside of the pselect and exec* resets sighandling.
  const int default_stdio_fds[]
    = { STDIN_FILENO, STDOUT_FILENO, STDERR_FILENO };
  int* stdio_fds = *stdio_fds_;

  for (int i = 0; i < 3; i++)
  {
    if (stdio_fds[i] != default_stdio_fds[i])
    {
      dup2 (stdio_fds[i], default_stdio_fds[i]);
      close (stdio_fds[i]);
    }
  }

  execvp (argv[0], argv);
  perror ("execvp");
  exit (1);
}

// returns 0 on success, -1 on malloc failure, -2 on pipe failure, -3 on fork
// failure
int fork_command_pipeline
  ( int stdin
  , int stdout
  , int stderr
  , int process_count
  , char*** argvs
  , pid_t* pids
  )
{
  int status;
  // The dimension is 2 since pipelines only redirect stdin's and stdout's of
  // each process.
  int (*stdio_fds)[2];

  assert (process_count > 0);

  stdio_fds = malloc (process_count * sizeof *stdio_fds);
  if (stdio_fds == NULL)
    return -1;

  stdio_fds[0][0] = stdin;
  for (int i = 0; i < process_count-1; i++)
  {
    int pipe_fds[2];

    status = pipe (pipe_fds);
    if (status == -1)
      return (free (stdio_fds), -2);

    printf ("[srv] opened pipe: (%i,%i)\n", pipe_fds[0], pipe_fds[1]);

    ((int*)stdio_fds)[1 + 2*i]     = pipe_fds[1];
    ((int*)stdio_fds)[1 + 2*i + 1] = pipe_fds[0];
  }
  stdio_fds[process_count-1][1] = stdout;

  printf ("[srv] stdio_fds: ");
  for (int i = 0; i < process_count; i++)
    printf ("(%i,%i) , ", stdio_fds[i][0], stdio_fds[i][1]);
  printf ("\n");

  for (int i = 0; i < process_count; i++)
  {
    pid_t pid;
    int process_stdio_fds[3] = {stdio_fds[i][0], stdio_fds[i][1], stderr};

    pid = fork ();
    if (pid == -1)
    {
      printf ("[shell] fork external command failed\n");
      return (free (stdio_fds), -3);
    }

    if (pid == 0)
      execute_external_command (&process_stdio_fds, argvs[i]);

    // Assume we are in the parent.
    assert (pid > 0);

    printf ("[srv] forked children with pid %i\n", pid);

    //TODO errcheck?
    // Close the pipes since the parent doesn't need them.
    if (i != 0)
      close (stdio_fds[i][0]);
    if (i != process_count-1)
      close (stdio_fds[i][1]);

    if (pids != NULL)
      pids[i] = pid;
  }

  free (stdio_fds);

  return 0;
}
