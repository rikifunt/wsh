#ifndef PARSER_H
#define PARSER_H


// builtin_index is -1 if no builtin parsed; return value is -1 if internal
// malloc failed, -2 if parse error, 0 otherwise; parse_error_message is not
// NULL when a parse error/warning should be displayed (error if ret val is -2,
// warning otherwise); it should not be freed. If ret val is negative (i.e.
// either malloc or parse error) argvs, argcs etc. have no meaning and are not
// guaranteed to be NULL (obv. nothing is malloc'ed in these cases). NOTE: empty
// lines are invalid and will return -2; thus, argvs and argcs will always have
// at least 1 element when ret val is 0.
int parse_command_line ( const char*
                       , int*
                       , int*
                       , int* *
                       , char* ***
                       , char* *
                       , char* *
                       , char* *
                       , const char* *
                       );


#endif // PARSING_PRIMITIVES_H
