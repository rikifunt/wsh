// Make sigaction & co. and pselect available.
#define _POSIX_C_SOURCE 200809L

#include <stdio.h> // printf
#include <stdlib.h> // free, exit
#include <string.h> // strlen, [...]
#include <errno.h> // errno
#include <assert.h> // assert
#include <stdint.h> // uint16_t
#include <unistd.h> // isatty
#include <termios.h> // struct termios, tcgetattr, tcsetattr, ECHO, TCSAFLUSH
#include <sys/select.h> // pselect, FD_*, fd_set
#include <sys/types.h> // connect, socket
#include <signal.h> // sigemptyset, sigset_t
#include <sys/socket.h> // connect, socket, AF_INET, SOCK_STREAM, PF_INET
#include <arpa/inet.h> // hton*, ntoh*, inet_addr, in_addr_t
#include <netinet/in.h> // sockaddr_in


#include <readline/readline.h>
#include <readline/history.h>

#include "utils/fd_io.h" // write_chars
#include "msg_io.h" // enum msg_type, recv_msg, send_msg


void terminate_session_coop (int, char);

// These globals are implicitely used by signal handlers,
// 'send_command_line_response' and 'main'; other uses are explicit (i.e. the
// address or the value of these variables is passed as an argument).
static int server_socket;
static int is_reading_command_line = 0;


int main (int argc, char* argv[])
{
  int   connect_to_server    (const char*, uint16_t);
  void  authenticate_user    (int);
  void  recv_and_process_msg (int, int*);

  int status;
  sigset_t empty_sigset;
  const char* srv_ip_addr = "127.0.0.1";
  int16_t     srv_port    = 9000;

  sigemptyset (&empty_sigset);

  // Parse command line arguments.
  {
    if (argc != 3)
    {
      fprintf(stderr, "[cli] wrong number of arguments (2 expected)\n");
      exit (1);
    }

    srv_ip_addr = argv[1];

    char* port_str;
    char* leftover;
    int   port;

    port_str = argv[2];

    errno = 0;
    port = strtol (port_str, &leftover, 0);
    if (errno == ERANGE || *leftover != 0 || (port > 65535 || port < 0))
    {
      fprintf (stderr, "the port argument does not represent a valid port number\n");
      exit (1);
    }
    srv_port = port;
  }

  //TODO how to uninstall the rl handler before exit without cluttering code
  //with cumbersome ifs? use a wrapper to exit that handles all exit procedures

  server_socket = connect_to_server (srv_ip_addr, srv_port);

  //TODO wait to be foreground if stdin is a terminal

  authenticate_user (server_socket);

  while (1)
  {
    fd_set server_socket_and_stdin;

    FD_ZERO (&server_socket_and_stdin);
    FD_SET (server_socket, &server_socket_and_stdin);
    FD_SET (STDIN_FILENO, &server_socket_and_stdin);

    // Atomically unblock all signals for the duration of the call to ensure
    // signals are only actually handled right after the call (and thus they do
    // not interfere with the main loop logic).
    do status = pselect ( FD_SETSIZE
                        , &server_socket_and_stdin
                        , NULL, NULL, NULL
                        , &empty_sigset
                        );
    while (status == -1 && errno == EINTR);
    if (status == -1)
    {
      fprintf (stderr, "[cli] pselect failed\n");
      terminate_session_coop (server_socket, 1);
    }

    if (FD_ISSET (server_socket, &server_socket_and_stdin))
    {
      recv_and_process_msg (server_socket, &is_reading_command_line);
    }

    if (FD_ISSET (STDIN_FILENO, &server_socket_and_stdin))
    {
      if (is_reading_command_line)
      {
        rl_callback_read_char ();
      }
      else
      {
        const int forwarding_buffer_size = 64;
        char forwarding_buffer[forwarding_buffer_size];
        int read_count;

        // NOTE: EINTR checking can't be avoided since some unhandled signal may
        // be received during the call.
        do read_count = read ( STDIN_FILENO
                             , forwarding_buffer
                             , forwarding_buffer_size - 1
                             );
        while (read_count == -1 && errno == EINTR);

        if (read_count == -1)
        {
          fprintf (stderr, "[cli] failed to read from stdin\n");
          terminate_session_coop (server_socket, 1);
        }

        if (read_count > 0)
        {
          status = send_msg ( server_socket
                              , MSG_STDIN
                              , read_count
                              , forwarding_buffer
                              );
          if (status < 0)
          {
            fprintf (stderr, "[cli] failed to send stdin\n");
            exit (1);
          }
        }
        else if (read_count == 0)
        {
          const char zero_char = 0;

          printf ("[user] EOF\n");

          status = send_msg (server_socket, MSG_EOF, 1, &zero_char);
          if (status < 0)
          {
            fprintf (stderr, "[cli] failed to send EOF\n");
            exit (1);
          }
        }
      }
    }
  }

  assert ("it should not be possible to reach this statement" == NULL);
}


int connect_to_server (const char* ip_address, uint16_t port)
{
  int status;
  struct sockaddr_in server_address;
  int server_socket;

  server_socket = socket (PF_INET, SOCK_STREAM, 0);
  if (server_socket == -1)
  {
    fprintf (stderr, "[cli] failed to open socket\n");
    exit (1);
  }

  server_address.sin_addr.s_addr = inet_addr (ip_address);
  if (server_address.sin_addr.s_addr == (in_addr_t)-1)
  {
    fprintf (stderr, "[cli] invalid ip addr\n");
    exit (1);
  }

  server_address.sin_family      = AF_INET;
  server_address.sin_port        = htons (port);

  status = connect ( server_socket
                   , (struct sockaddr*)&server_address
                   , sizeof server_address
                   );
  if (status == -1)
  {
   fprintf (stderr, "[cli] failed to open socket\n");
   exit (1);
  }

  return server_socket;
}


void authenticate_user (int server_socket)
{
  char* readline_for_authentication (int, char*);

  int status;
  char is_user_authenticated = 0;

  while (!is_user_authenticated)
  {
    char* pam_prompt_response = NULL;
    uint32_t msg_type;
    uint32_t msg_length;
    char* msg_payload;

    status = recv_msg (server_socket, &msg_type, &msg_length, &msg_payload);
    if (status < 0)
    {
      fprintf (stderr, "[cli] failed to recv message\n");
      exit (1);
    }

    switch (msg_type)
    {
      case MSG_PAM_AUTHENTICATION_STATUS:
        is_user_authenticated = *msg_payload;
        if (!is_user_authenticated)
        {
          fprintf (stderr, "[cli] failed to authenticate user\n");
          exit (1);
        }
        break;

      case MSG_PAM_PROMPT_ECHO_OFF:
      case MSG_PAM_PROMPT_ECHO_ON:
        pam_prompt_response =
          readline_for_authentication
            (msg_type == MSG_PAM_PROMPT_ECHO_ON, msg_payload);

        status = send_msg ( server_socket
                          , MSG_PAM_PROMPT_RESPONSE
                          , 1 + strlen (pam_prompt_response)
                          , pam_prompt_response
                          );
        if (status < 0)
        {
          fprintf (stderr, "failed to send pam prompt response\n");
          exit (1);
        }

        free (pam_prompt_response);

        break;

      default:
        fprintf (stderr, "[cli] wrong message type\n");
        exit (1);
    }

    free (msg_payload);
  }

  printf ("[cli] authentication successful, session starting...\n");
}


char* readline_for_authentication (int is_echoing_on, char* prompt)
{
  int status;
  char* line;
  struct termios old_terminal_attrs, new_terminal_attrs;

  if (!is_echoing_on)
  {
    status = tcgetattr (STDIN_FILENO, &old_terminal_attrs);

    if (status != 0)
    {
      fprintf (stderr, "[cli] warning: failed to disable echoing on terminal\n");
    }

    new_terminal_attrs = old_terminal_attrs;
    new_terminal_attrs.c_lflag &= ~ECHO;

    status = tcsetattr (STDIN_FILENO, TCSAFLUSH, &new_terminal_attrs);
    if (status != 0)
    {
      fprintf (stderr, "[cli] failed to set term setting\n");
    }
  }

  line = readline (prompt);
  if (line == NULL)
    do
    {
      fprintf (stderr, "[cli] cannot accept EOF as response\n");
      line = readline (prompt);
    }
    while (line == NULL);

  if (!is_echoing_on)
  {
    printf ("\n");

    status = tcsetattr (STDIN_FILENO, TCSAFLUSH, &old_terminal_attrs);
    if (status != 0)
      fprintf (stderr, "[cli] warning: unable to restore terminal attributes, echoing will likely remain disabled\n");
  }

  return line;
}


void recv_and_process_msg
  (int server_socket, int* is_reading_command_line)
{
  void send_command_line_response (char*);

  int status;
  uint32_t msg_type;
  uint32_t msg_length;
  char*    msg_payload;

  status = recv_msg (server_socket, &msg_type, &msg_length, &msg_payload);
  if (status < 0)
  {
    fprintf (stderr, "[cli] failed to recv message\n");
    exit (1);
  }

  switch (msg_type)
  {
    case MSG_EXIT:
      exit (*msg_payload);
      break;

    case MSG_COMMAND_LINE_REQUEST:
      if (*is_reading_command_line)
      {
        fprintf (stderr, "[cli] unexpected command line request\n");
        exit (1);
      }
      *is_reading_command_line = 1;
      rl_callback_handler_install ("> ", send_command_line_response);
      break;

    case MSG_STDOUT:
    case MSG_STDERR:
      status = write_chars (msg_type == MSG_STDOUT ? STDOUT_FILENO : STDERR_FILENO, msg_length, msg_payload);
      if (status == -1)
      {
        fprintf (stderr, "[cli] failed to forward stdout/stderr to user\n");
      }
      break;

    default:
      fprintf (stderr, "[cli] unknown message type\n");
      exit (1);
  }

  free (msg_payload);
}


void send_command_line_response (char* command_line)
{
  int status;

  rl_callback_handler_remove ();
  is_reading_command_line = 0;

  if (command_line == NULL)
  {
    terminate_session_coop (server_socket, 0);
  }

  if (*command_line == 0)
  {
    is_reading_command_line = 1;
    rl_callback_handler_install ("> ", send_command_line_response);
    free (command_line);
    return;
  }

  add_history (command_line);

  status = send_msg ( server_socket
                      , MSG_COMMAND_LINE_RESPONSE
                      , 1 + strlen (command_line)
                      , command_line
                      );
  if (status < 0)
  {
    fprintf (stderr, "[cli] failed to send command line\n");
    exit (1);
  }

  free (command_line);
}


void terminate_session_coop (int server_socket, char exit_status)
{
  int status;

  status = send_msg (server_socket, MSG_EXIT, 1, &exit_status);
  if (status < 0)
  {
    fprintf (stderr, "failed to send exit message to server\n");
  }

  exit (exit_status);
}
