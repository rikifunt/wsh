#ifndef COMMANDS_EXECUTION_H
#define COMMANDS_EXECUTION_H

#include <sys/types.h> // pid_t

extern const int exit_builtin_index;

int get_builtin_index (const char*);

// valid values for the index are all values returned by
// shell_parse_command_line but -1; the index is NOT checked for validity. this
// call is blocking (i.e. it returns only when the builtin terminates). NOTE:
// the exit builtin does NOT actually exit the program; it only prints the
// message "exit". The caller should intercept exit messages by checking for
// exit_builtin_index and do whatever it needs to do to exit the shell.
int execute_builtin (int, void(*)(int, const char*), int, char**);

int fork_command_pipeline
  ( int
  , int
  , int
  , int
  , char***
  , pid_t*
  );


#endif // COMMANDS_EXECUTION_H
