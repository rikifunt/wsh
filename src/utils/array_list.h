#ifndef ARRAY_LIST_H
#define ARRAY_LIST_H


struct array_list;

struct array_list* array_list_alloc (void);
// NOTE: frees both elements and the struct itself
void               array_list_free (struct array_list*);
int                array_list_init (struct array_list*, int, int);
// NOTE: does NOT free anything on failure
int                array_list_push (struct array_list*, char*);
// gets the address of the first element
char*              array_list_get_elements (struct array_list*);


#endif // ARRAY_LIST_H
