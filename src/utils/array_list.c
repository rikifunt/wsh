#include <stdlib.h> // malloc
#include <string.h> // memcpy


#include "array_list.h"


struct array_list
  { int   size_of_one_element
  ; int   capacity
  ; int   element_count
  ; char* elements
  ;};


struct array_list* array_list_alloc (void)
{
  struct array_list* array_list;

  array_list = malloc (sizeof *array_list);
  if (array_list == NULL) return NULL;

  return array_list;
}

void array_list_free (struct array_list* array_list)
{
  free (array_list->elements);
  free (array_list);
}

int array_list_init
  ( struct array_list* array_list
  , int                size_of_one_element
  , int                initial_capacity
  )
{
  array_list->size_of_one_element = size_of_one_element;
  array_list->capacity            = initial_capacity;
  array_list->element_count       = 0;

  array_list->elements = malloc (size_of_one_element * initial_capacity);
  if (array_list->elements == NULL) return -1;

  return 0;
}

char* array_list_get_elements (struct array_list* array_list)
{
  return array_list->elements;
}

int array_list_push (struct array_list* array_list, char* new_element)
{
  if (1 + array_list->element_count > array_list->capacity)
  {
    char* new_elements;

    array_list->capacity *= 2;
    new_elements = realloc ( array_list->elements
                           ,   array_list->size_of_one_element
                             * array_list->capacity
                           );
    if (new_elements == NULL)
      return -1;

    array_list->elements = new_elements;
  }

  memcpy ( &(array_list
               ->elements[   array_list->size_of_one_element
                           * array_list->element_count
                         ]
            )
         , new_element
         , array_list->size_of_one_element
         );
  array_list->element_count++;

  return 0;
}
