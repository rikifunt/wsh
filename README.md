# Building executables

Invoking `make` at the top directory builds both the server side executable
`wshs` and the client side executable `wsh`.

The following C libraries are needed to build the client side executable:

- [GNU readline](https://tiswww.case.edu/php/chet/readline/rltop.html)


The following C libraries are needed to build the server side executable:

- PAM, on systems that don't ship it by default, e.g.:

  - [Linux-PAM](http://www.linux-pam.org/)

  Note that e.g. on GNU/linux systems an appropriate service file
  `/etc/pam.d/wsh.conf` has to be created to allow the server side to use PAM; a
  minimal version would contain at least the following lines:

```
auth       required     pam_unix.so
account    required     pam_unix.so
```


# Syntax for Command Lines

The syntax if very similar to that of bash: pipelines are obtained with the `|`
operator, background commands with the `&` (postfix) operator, and arguments and
command names are separated by whitespace. Double quotes, however, escape
everything inside them (even backslashes); single quotes do NOT have any special
meaning. Backslashes, if outside quotes, escape the next character as usual. The
following lines are examples of valid command lines.

```
ps aux | grep wsh
cd "dir with spaces in its name"
cd dir\ with\ spaces\ in\ its\ name
gcc main.c &
```
