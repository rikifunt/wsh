# top level makefile for wsh

all: wsh wshd wsh-http

thesis.pdf: thesis.tex
	pdflatex $<
	pdflatex $< # compiled twice to make the TOC
	rm thesis.aux thesis.log thesis.out thesis.toc

wsh:
	cd src && make wsh
	cp src/wsh -t .

wshd:
	cd src && make wshd
	cp src/wshd -t .

wsh-http:
	cd src && make wsh-http
	cp src/wsh-http -t .


clean_files = wsh wshd wsh-http thesis.pdf
clean:
	cd src && make clean
	for file in $(clean_files); do ! test -f $${file} || rm $${file}; done


.PHONY: all clean wsh wshd wsh-http
